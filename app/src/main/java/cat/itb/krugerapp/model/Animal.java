package cat.itb.krugerapp.model;


import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.storage.StorageReference;

public class Animal {
    private String nombreAnimal;
    private LatLng latLng;
    private StorageReference reference;

    public StorageReference getReference() {
        return reference;
    }

    public void setReference(StorageReference reference) {
        this.reference = reference;
    }

    public Animal(String nombreAnimal, LatLng latLng, StorageReference reference) {
        this.nombreAnimal = nombreAnimal;
        this.latLng = latLng;
        this.reference = reference;
    }

    public String getNombreAnimal() {
        return nombreAnimal;
    }

    public void setNombreAnimal(String nombreAnimal) {
        this.nombreAnimal = nombreAnimal;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
