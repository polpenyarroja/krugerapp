package cat.itb.krugerapp.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;

import cat.itb.krugerapp.AnadirUbicacionActivity;
import cat.itb.krugerapp.KrugerMapActivity;
import cat.itb.krugerapp.KruggerAppViewModel;
import cat.itb.krugerapp.R;

public class StartFragment extends Fragment {

    private KruggerAppViewModel mViewModel;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(KruggerAppViewModel.class);

        MaterialButton verMapa_btn = getView().findViewById(R.id.btnInicio);
        verMapa_btn.setOnClickListener(this::changeToMapView);

        MaterialButton prova_btn = getView().findViewById(R.id.prova_btn);
        prova_btn.setOnClickListener(this::navigateToAdd);
    }

    private void navigateToAdd(View view) {
        Intent intent = new Intent(getActivity(), AnadirUbicacionActivity.class);
        startActivity(intent);
    }

    private void changeToMapView(View view) {
        Intent intent = new Intent(getActivity(), KrugerMapActivity.class);
        startActivity(intent);
    }

}
