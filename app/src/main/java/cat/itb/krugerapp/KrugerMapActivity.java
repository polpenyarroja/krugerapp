package cat.itb.krugerapp;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;
import java.util.ArrayList;
import java.util.List;
import cat.itb.krugerapp.model.Animal;

public class KrugerMapActivity extends FragmentActivity implements OnMapReadyCallback {

    //Atributos
    private KruggerAppViewModel mViewModel;
    private GoogleMap mMap;
    private List<Animal> animals=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(KruggerAppViewModel.class);
        setContentView(R.layout.activity_kruger_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Listener del botón para ir a la cámara
        FloatingActionButton camera_btn = findViewById(R.id.camera_btn);
        camera_btn.setOnClickListener(this::toCameraIntent);
        //Inicializa storage de firebase
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference listRef = storage.getReference().child("images");
        //Lista de las imagenes guardades en la carpeta images
        listRef.listAll()
                .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                    int i = 0;
                    @Override
                    public void onSuccess(ListResult listResult) {
                        for (StorageReference prefix : listResult.getPrefixes()) {
                            // All the prefixes under listRef.
                            // You may call listAll() recursively on them.
                        }

                        for (StorageReference item : listResult.getItems()) {
                            //Coge el nombre de cada archivo y lo separa para poder crear un objeto animal
                            String name=item.getName().split("_")[0];
                            String ubicacion1=item.getName().split("_")[1];
                            String ubicacion2=item.getName().split("_")[2];
                            double ubicacion1Double = Double.valueOf(ubicacion1);
                            double ubicacion2Double = Double.valueOf(ubicacion2);
                            //Creación del animal con todos los atributos
                            Animal animal=new Animal(name,new LatLng(ubicacion1Double, ubicacion2Double),item);
                            animals.add(animal);
                            System.out.println(item.getName());
                            //Dependiendo del animal que sea cargará un marcador con la imagen del animal
                            if(name.equals("León")){
                                mMap.addMarker(new MarkerOptions()
                                        .position(animal.getLatLng())
                                        .icon(mViewModel.bitmapDescriptorFromVector(getBaseContext(), R.drawable.ic_roundicons_100_free_solid_lion))
                                        .title(animal.getNombreAnimal())).setTag(i);
                            }
                            if(name.equals("Jirafa")){
                                mMap.addMarker(new MarkerOptions()
                                        .position(animal.getLatLng())
                                        .icon(mViewModel.bitmapDescriptorFromVector(getBaseContext(), R.drawable.ic_silueta_de_jirafa))
                                        .title(animal.getNombreAnimal())).setTag(i);
                            }
                            if(name.equals("Elefante")){
                                mMap.addMarker(new MarkerOptions()
                                        .position(animal.getLatLng())
                                        .icon(mViewModel.bitmapDescriptorFromVector(getBaseContext(), R.drawable.ic_animales_salvajes))
                                        .title(animal.getNombreAnimal())).setTag(i);
                            }
                            if(name.equals("Cocodrilo")){
                                mMap.addMarker(new MarkerOptions()
                                        .position(animal.getLatLng())
                                        .icon(mViewModel.bitmapDescriptorFromVector(getBaseContext(), R.drawable.ic_caiman))
                                        .title(animal.getNombreAnimal())).setTag(i);
                            }
                            if(name.equals("Hipopotamo")){
                                mMap.addMarker(new MarkerOptions()
                                        .position(animal.getLatLng())
                                        .icon(mViewModel.bitmapDescriptorFromVector(getBaseContext(), R.drawable.ic_hipopotamo))
                                        .title(animal.getNombreAnimal())).setTag(i);
                            }
                            if(name.equals("Guepardo")){
                                mMap.addMarker(new MarkerOptions()
                                        .position(animal.getLatLng())
                                        .icon(mViewModel.bitmapDescriptorFromVector(getBaseContext(), R.drawable.ic_leopardo))
                                        .title(animal.getNombreAnimal())).setTag(i);
                            }
                            i=i+1;
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Uh-oh, an error occurred!
                    }
                });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-24.744320, 31.698156), 9));

        //Dibuja en el mapa un polígono que representa la zona de kruger
        Polyline polyline1 = googleMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(
                        new LatLng(-23.996273, 31.879431),
                        new LatLng(-24.181896, 31.906211),
                        new LatLng(-24.302167, 31.981566),
                        new LatLng(-24.458313, 32.009091),
                        new LatLng(-24.690601, 31.995718),
                        new LatLng(-25.385138, 32.013492),
                        new LatLng(-25.434632, 31.988301),
                        new LatLng(-25.331436, 31.914131),
                        new LatLng(-25.378842, 31.889035),
                        new LatLng(-25.364947, 31.865400),
                        new LatLng(-25.375358, 31.852154),
                        new LatLng(-25.363395, 31.816413),
                        new LatLng(-25.332400, 31.815853),
                        new LatLng(-25.331097, 31.779214),
                        new LatLng(-25.301125, 31.774177),
                        new LatLng(-25.314558, 31.743763),
                        new LatLng(-25.382162, 31.752395),
                        new LatLng(-25.376604, 31.709390),
                        new LatLng(-25.437277, 31.619367),
                        new LatLng(-25.398774, 31.620006),
                        new LatLng(-25.410268, 31.560728),
                        new LatLng(-25.448012, 31.515789),
                        new LatLng(-25.463674, 31.533934),
                        new LatLng(-25.519866, 31.373911),
                        new LatLng(-25.488172, 31.345836),
                        new LatLng(-25.494964, 31.336952),
                        new LatLng(-25.488231, 31.332415),
                        new LatLng(-25.485908, 31.324568),
                        new LatLng(-25.429719, 31.314843),
                        new LatLng(-25.414559, 31.302140),
                        new LatLng(-25.372018, 31.297475),
                        new LatLng(-25.358533, 31.277717),
                        new LatLng(-25.340929, 31.269452),
                        new LatLng(-25.314021, 31.258707),
                        new LatLng(-25.281407, 31.262843),
                        new LatLng(-25.258551, 31.246591),
                        new LatLng(-25.248233, 31.254770),
                        new LatLng(-25.213607, 31.236986),
                        new LatLng(-25.210168, 31.225081),
                        new LatLng(-25.088805, 31.197809),
                        new LatLng(-25.047053, 31.232388),
                        new LatLng(-25.015661, 31.253570),
                        new LatLng(-24.982288, 31.253621),
                        new LatLng(-24.977758, 31.487430),
                        new LatLng(-24.935375, 31.442856),
                        new LatLng(-24.935375, 31.442856),
                        new LatLng(-24.858682, 31.470915),
                        new LatLng(-24.865530, 31.387623),
                        new LatLng(-24.847807, 31.389167),
                        new LatLng(-24.733631, 31.329691),
                        new LatLng(-24.734230, 31.463955),
                        new LatLng(-24.671950, 31.594513),
                        new LatLng(-24.609671, 31.599521),
                        new LatLng(-24.556498, 31.450292),
                        new LatLng(-24.524747, 31.425078),
                        new LatLng(-24.480870, 31.471763),
                        new LatLng(-24.480870, 31.471763),
                        new LatLng(-24.539075, 31.371946),
                        new LatLng(-24.553248, 31.228848),
                        new LatLng(-24.498188, 31.254840),
                        new LatLng(-24.421943, 31.202772),
                        new LatLng(-24.434313, 31.161529),
                        new LatLng(-24.377614, 31.163243),
                        new LatLng(-24.364048, 31.247310),
                        new LatLng(-24.302186, 31.201621),
                        new LatLng(-24.318826, 31.058733),
                        new LatLng(-24.186898, 31.052058),
                        new LatLng(-24.100956, 31.252543),
                        new LatLng(-23.994264, 31.871350),
                        new LatLng(-23.996273, 31.879431)
                    ));
        //Ancho de la linea
        polyline1.setWidth(5);

        //listener del marker
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //Dependiendo del marcador que haya clickado cargara un alertDialog con la información de ese animal (nombre, ubicación e imagen)
                int position = (int)(marker.getTag());
                StorageReference storageReference = FirebaseStorage.getInstance().getReference();
                StorageReference photoReference= storageReference.child("images/"+animals.get(position).getNombreAnimal()+"_"+animals.get(position).getLatLng().latitude+"_"+animals.get(position).getLatLng().longitude+"");
                ImageView image = new ImageView(KrugerMapActivity.this);
                image.setImageResource(R.drawable.kruger3daysafari);
                //Crea el alert dialog con los datos necesareos
                AlertDialog alertDialog = new AlertDialog.Builder(KrugerMapActivity.this).create();
                alertDialog.setTitle(animals.get(position).getNombreAnimal()+" encontrado/a en "+ animals.get(position).getLatLng().latitude+", "+animals.get(position).getLatLng().longitude);
                alertDialog.setView(image);
                RequestOptions requestOptions = new RequestOptions();
                //Muestra la imagen en la imageView
                photoReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Glide
                        .with(KrugerMapActivity.this)
                        .load(uri)
                        .apply(requestOptions)
                        .into(image);
                    }
                });
                //Cierra el alertDialog
                alertDialog.setButton("Continuar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
                return false;
            }
        });


    }

    private void toCameraIntent(View view) {
        //Navega hacia la cámara
        Intent intent = new Intent(this, AnadirUbicacionActivity.class);
        startActivity(intent);
    }
}
