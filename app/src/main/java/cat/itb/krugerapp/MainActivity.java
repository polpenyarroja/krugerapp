package cat.itb.krugerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cat.itb.krugerapp.ui.main.StartFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

    }
}
