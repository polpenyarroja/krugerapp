# KrugerApp
#### Noms:

- Sergi Díaz
- Pol Peñarroja

#### Ítems:

- API Maps, marcadors i polylines
- Firebase Storage
- Latitud i longitud (locationClient)
- Imatges (drawable, bitmap)
- Càmera

#### Extras:

- Extra relacionat amb el mapa: icones d'animals com a marcadors i polyline delimitant la zona del parc
- Extra relacionat amb imatges o multimedia: cargar imatge al fer foto i cargar imatge desde firebase storage

#### Trello:
- https://trello.com/b/96p2wxGO/krugerapp
